
# Add my home bin dir
export PATH=$PATH:$HOME/.local/bin:$HOME/.gem/ruby/2.3.0/bin

# This makes Arch so much easier at times.
source /usr/share/doc/pkgfile/command-not-found.zsh

### Antigen Configuration and Bundles ###
source .antigen/antigen.zsh

antigen bundle archlinux
antigen bundle Tarrasch/zsh-autoenv
antigen bundle aws
antigen bundle docker
antigen bundle gitfast
antigen bundle sudo
antigen bundle tmux
antigen bundle zsh-users/zsh-syntax-highlighting

# Start up powerline and init the theme
powerline-daemon -q
. /usr/lib/python3.5/site-packages/powerline/bindings/zsh/powerline.zsh

# Custom aliases
alias pbcopy='xsel --clipboard --input'
alias pbpaste='xsel --clipboard --output'
alias ls='ls --color=auto'

# dotfiles management
alias dotfiles='/usr/bin/git --git-dir=$HOME/.files/ --work-tree=$HOME'

# Correct keybindings
bindkey "${terminfo[khome]}" beginning-of-line
bindkey "${terminfo[kend]}" end-of-line

gpg-agent --daemon -q &> /dev/null
export SSH_AUTH_SOCK=$XDG_RUNTIME_DIR/gnupg/S.gpg-agent.ssh
export GPG_AGENT_INFO=$XDG_RUNTIME_DIR/gnupg/S.gpg-agent

SAVEHIST=1000
HISTFILE=~/.zsh_history 
