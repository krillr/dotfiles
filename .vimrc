set modeline
set  rtp+=/usr/lib/python2.7/site-packages/powerline/bindings/vim/
set laststatus=2
set t_Co=256
set noshowmode
set noruler
set noshowcmd

autocmd Filetype javascript setlocal ts=2 sts=2 sw=2
autocmd Filetype coffeescript setlocal ts=2 sts=2 sw=2
autocmd Filetype html setlocal ts=2 sts=2 sw=2

syntax on
filetype indent plugin on
